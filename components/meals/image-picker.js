'use client';

import { useRef, useState } from 'react';
import classes from './image-picker.module.css';
import Image from 'next/image';

export default function ImagePicker({ label, name }) {
  const imageInputRef = useRef();
  const [pickedImage, setPickedImage] = useState();

  const handlePickClick = () => {
    imageInputRef.current.click();
  };

  const handleImageChange = (event) => {
    const file = event.target.files[0];

    if (!file) {
      setPickedImage(null);
      return;
    }
    const fileReader = new FileReader();
    fileReader.onload = () => {
      setPickedImage(fileReader.result);
    };
    fileReader.readAsDataURL(file);
  };

  console.log(pickedImage, 'picked');

  return (
    <div className={classes.picker}>
      <label htmlFor={name}>{label}</label>
      <div className={classes.controls}>
        <div className={classes.preview}>{pickedImage ? <Image src={pickedImage} fill alt='The image selected by the user.' /> : <p>No image picked yet</p>}</div>
        <input required ref={imageInputRef} className={classes.input} type='file' id={name} accept='image/png, image/jpeg' name={name} onChange={handleImageChange} />
        <button onClick={handlePickClick} className={classes.button} type='button'>
          Pick an Image
        </button>
      </div>
    </div>
  );
}
